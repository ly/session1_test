





## R实战题目

#### 从Gitlab上下载测试数据

````
git clone git@gitlab.com:ly/session1_test.git
````



#### 在Rstudio中设置项目路径

File -> New Project

#### 题目内容 ####

开发一个R模块，输入物种丰度表格和样品分组表格，画出不同物种在不同分组间的boxplot图（每个物种化成一张boxplot图）。



#### 题目要求 ####

1. 写成一个R脚本
2. 使用optparse包传入参数，脚本里面不能出现绝对现有的路径（提高代码的重复利用）
3. 调用多线程，在参数中指定多线程
4. 指定输出目录
5. 加载需要使用的包



#### 题目输入文件

| 输入                 | 信息                                                         |
| -------------------- | ------------------------------------------------------------ |
| 物种丰度表格（输入） | 在git目录的data目录下（或/home/liangyong/gitlab/r_session1_test/data/tax.xls） |
| 样品分组表格（输入） | 在git目录的data目录下（或/home/liangyong/gitlab/r_session1_test/data/mapping.txt） |

物种丰度表部分展示如下：

```shell
$head /home/liangyong/gitlab/r_session1_test/data/tax.xls
Taxon   DYEY1   DYEY10  DYEY3   DYEY4   DYEY5   DYEY6   DYEY2   DYEY7   DYEY8
Deltaproteobacteria     88      76      473     238     0       61      421     107     23
Bacteroidia     34505   22557   39246   8222    60988   26490   22196   111926  17352
Fusobacteriia   10      0       5       0       5       0       0       0       14
Cytophagia      0.0     0.0     0.0     0.0     3.0     0.0     0.0     0.0     0.0
Clostridia      53410   30091   87890   133183  41990   71800   114306  121264  61185
Negativicutes   2057    1039    2804    1771    3117    1675    2869    622     605
Erysipelotrichia        3160    151     343     3014    1605    1120    2295    5488    6529
Alphaproteobacteria     6       2       3       6       0       7       0       4       4
Betaproteobacteria      12      258     512     27      5082    135     147     116     932
```

样品分组列表展示如下：

```shell
$cat /home/liangyong/gitlab/r_session1_test/data/mapping.txt
Sample  Group1  Gender
DYEY1   AA      M
DYEY10  AA      M
DYEY3   AA      F
DYEY4   Aa      F
DYEY5   Aa      F
DYEY6   Aa      F
DYEY2   Ab      F
DYEY7   Ab      M
DYEY8   Ab      F


```



#### 最终结果界面

Rscrip脚本界面如下，下列参数市必须的：

```shell
#Rscript脚本help界面，以及必须的参数
$Rscript Main.R  -h
Usage: This Script is a test for arguments!


Options:
        -t TABLE, --table=TABLE
                Input the taxonomy abundance table

        -d DIR, --dir=DIR
                Input the dir output to

        -p PROCESS, --process=PROCESS
                set the process to use

        -m MAPPING, --mapping=MAPPING
                Input the group file

        -h, --help
                Show this help message and exit
```



#### 最终脚本运行方式

```{shell}
Rscript /home/liangyong/gitlab/r_session1_test/scripts/Main.R -t /home/liangyong/gitlab/r_session1_test/data/tax.xls  -d /home/liangyong/gitlab/r_session1_test/figure -p 2 -m /home/liangyong/gitlab/r_session1_test/data/mapping.txt
```



#### 其中一个物种的丰度展示

![image.png](https://i.loli.net/2020/09/24/7XfAquNCSdisWxc.png)



#### 进行单元测试

直接使用git下的文件进行测试



#### 测试完成后写README

写README使用 markdown格式



#### 将写的代码上传到git

首先编写 .gitigonre文件，过滤不想上传的目录或者文件

```
git checkout -b liangyong
git add .
git commit -t "更新的内容"
git push origin liangyong
```










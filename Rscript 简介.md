## Rscript常规使用介绍



### 目录

---

- 参数传递
  + __commandArgs()__
  + optparse
  + __getopt__

- 数据保存
  - cat函数
  - __保存文本文件__
  - __保存为R文件__
- 图片保存
  - png函数
  - pdf函数
  - __ggsave__
- 一页多图
  - eaGgplot2包
  - __ggarrange__
  - __patchwork__(超强推荐)
- Rscript的优势

---



#### 在Linux command line运行R代码方法

```{shell}
Rscript test.R
```



#### 将R脚本写进一个R文件

将在Rstduio中运行的R代码，写进R文件，可以直接使用Rscript运行。下面从参数参数传递、表格输出、图片输出，以及命令行运行的优势来讲解。

###### 参数传递

参数传递有三种方式，分别为：commandArgs()，getopt和optparse

1. commandArgs()

   这是个`R`的**内置命令**，和`perl`的`@ARGV`或者和`python`的`sys.argv`类似，就是将来自于命令的参数存入向量（数组）中。但是与`perl`和`python`的不同，它的前面几个元素不是命令行的参数，先尝试打印一下这个参数是怎样的。

   下面是一个测试脚本`test.R`

   ```bash
   # commandArgs()就返回一个包含参数信息的向量
   args <- commandArgs()
   print(args)
   ```

   开始测试

   ```bash
   Rscript test_args.R Hello R
   ```

   输出为

   ```json
   [1] "C:\\PROGRA~1\\R\\R-35~1.2\\bin\\x64\\Rterm.exe"
   [2] "--slave"
   [3] "--no-restore"
   [4] "--file=test_args.R"
   [5] "--args"
   [6] "Hello"
   [7] "R"
   ```

   参数解释：

   | 位置 | 说明            | 示例                                   |
   | ---- | --------------- | -------------------------------------- |
   | 1    | R所在的路径     | C:\PROGRA1\R\R-351.2\bin\x64\Rterm.exe |
   | 2    | Rscript的参数   | --slave                                |
   | 3    | Rscript的参数   | --no-restore                           |
   | 4    | 运行R脚本的路径 | --file=test_args.R                     |
   | 5    | 脚本参数flag    | --args                                 |
   | 6    | 第一个参数      | Hello                                  |
   | 7    | 第二个参数      | R                                      |

   那么就是说输入的参数是从第**6**个开始（注意！R的索引与python或者perl不同，它是从**1**开始的。）

   其实对于参数的位置是可变的，在**R所在路径**和**R脚本的路径**这两个参数之间是Rscript的参数，这些参数的数量是**可变的**（其实这里也就是是按照顺序的`Rscript`的参数，后面的脚本其实也就是一个参数而已，这里看来和python或者perl是差不多的。）

   ```r
   2和3这两个参数虽然我们没有输入，但是是作为Rscript的默认参数的。
   
   参数索引  1       2       3            4                  5      6     7
            Rscript --slave --no-restore --file=test_args.R --args Hello R
   ```

   **这样一来就会导致R脚本的参数的索引不固定，针对这种情况，你也可以添加一个参数来削掉R脚本参数之前的参数了。**

   

   ```php
   args <- commandArgs(trailingOnly = TRUE)
   print(args)
   ```

   输出为

   ```json
   [1] "Hello"
   [2] "R"
   ```

   这样R脚本的参数就从`1`开始了。

   但是这样对于参数解析的顺序是需要的，也就是说脚本接受的参数的输入需要严格按照顺序来，有时候为了区分参数使用这个命令其实是不方便的。使用特定的flag来区分参数是需要，当然R也是有相应的包来做这些事情的。

   

   2. getopt

   在使用之前需要安装对应的包：

   ```r
   install.packages("getopt")
   ```

   这个包的`getopt()`函数使用方法是

   ```objectivec
   getopt(spec = NULL, opt = commandArgs(TRUE),command = get_Rscript_filename(), usage = FALSE,debug = FALSE)
   ```

   - `spec`：这个参数需要一个矩阵，这个矩阵描述了接受参数的flag以及参数的形式等等信息

   | 列   | 第一列                   | 第二列                   | 第三列                                                       | 第四列                                                       | 第五列                            |
   | ---- | ------------------------ | ------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | --------------------------------- |
   | 说明 | 参数的长名称（多个字符） | 参数的短名称（一个字符） | 这个flag对应的参数形式（0表示flag不接受参数；1表示可接可不接；2表示必须接参数） | 参数的类型（logical；integer；double；complex；character；numeric） | 注释信息                          |
   | 示例 | "first_arg"              | "f"                      | 2                                                            | "double"                                                     | "This arg is the first arguments" |

   - `opt`：表示参数的来源，一般都是使用默认的，也就是`commandArgs()`接受
   - `usage`：用法，就是说明帮助信息，默认为`FALSE`

   下面是一个示例，将下列代码存到`test.R`文件中：

   ```r
   library(getopt)
   
   # 首先将第一个参数的具体信息进行描述
   # 每行五个，第五个可选，也就是说第五列可以不写
   # byrow 按行填充矩阵的元素
   # ncol  每行填充五个元素
   spec <- matrix(
     c("first",  "f", 2, "integer", "This is first!",
       "second", "s", 1, "character",  "This is second!",
       "third",  "t", 2, "double",  "This is third!",
       "help",   "h", 0, "logical",  "This is Help!"),
     byrow=TRUE, ncol=5)
   
   # 使用getopt方法
   opt <- getopt(spec=spec)
   
   # opt实际上就是一个列表，直接使用$来索引到对应的参数的值
   print(opt$first)
   print(opt$second)
   print(opt$third)
   ```

   在命令行执行

   ```bash
   Rscript test.R -f 123 -t 1.1 -s Hello
   ```

   输出为：

   ```bash
   [1] 123
   [1] "Hello"
   [1] 1.1
   ```

   但是这里如果参数输出不正确的时候并不能弹出详细的帮助信息，下面增加一些信息就可以得到相应的帮助信息了

   ```r
   library(getopt)
   
   # 首先将第一个参数的具体信息进行描述
   spec <- matrix(
   # 每行五个，第五个可选，也就是说第五列可以不写
   # byrow 按行填充矩阵的元素
   # ncol  每行填充五个元素
     c("first",  "f", 2, "integer", "This is first!",
       "second", "s", 1, "character",  "This is second!",
       "third",  "t", 2, "double",  "This is third!",
       "help",   "h", 0, "logical",  "This is Help!"),
     byrow=TRUE, ncol=5)
   
   # 使用getopt方法，注意这里的usage默认是关闭的，这里不能打开
   opt <- getopt(spec=spec)
   
   # 这个时候需要将usage参数打开，这样getpot()就会返回一个特定写法的帮助文件
   # 当然你也可以自己写帮助，然后将if判断语句中的打印的信息换成你自己定义的帮助信息
   if( !is.null(opt$help) || is.null(opt$first) || is.null(opt$third) ){
       # ... 这里你也可以自定义一些东放在里面
       cat(paste(getopt(spec=spec, usage = T), "\n"))
       quit()
   }
   
   # opt实际上就是一个列表，直接使用$来索引到对应的参数的值
   print(opt$first)
   print(opt$second)
   print(opt$third)
   ```

   命令行执行

   ```r
   Rscript test.R -h
   ```

   输出为：

   ```csharp
   Usage: test_args.R [-[-first|f] [<integer>]] [-[-second|s] <character>] [-[-third|t] [<double>]] [-[-help|h]]
       -f|--first     This is first!
       -s|--second    This is second!
       -t|--third     This is third!
       -h|--help      This is Help!
   ```

   解析参数的包不止一个，R也不例外，这里也有另外一个包也是干同样的事情的，但是它的用法与上面的`getopt`包有一定差别。

   

   3. Opt parse

   使用之前安装包

   ```r
   install.packages("optparse")
   ```

   其中的方法`OptionParser()`的用法为：

   ```php
   OptionParser(usage = "usage: %prog [options]", option_list = list(),
     add_help_option = TRUE, prog = NULL, description = "",
     epilogue = "")
   ```

   其中最重要的参数是`option_list`，这个参数接受一个列表，这个列表是被用来描述命令参数的解析方式的。

   下面是一个例子，将下列代码存到`test.R`文件中：

   ```r
   library(optparse)
   
   # 描述参数的解析方式
   option_list <- list(
     make_option(c("-f", "--first"), type = "integer", default = FALSE,
                 action = "store", help = "This is first!"
     ),
     make_option(c("-s", "--second"), type = "character", default = FALSE,
                 action = "store", help = "This is second!"
     ),
     make_option(c("-t", "--third"), type = "double", default = FALSE,
                 action = "store", help = "This is third!"
     )
     # make_option(c("-h", "--help"), type = "logical", default = FALSE,
     #             action = "store_TRUE", help = "This is Help!"
     # )
   )
   
   # 解析参数
   opt = parse_args(OptionParser(option_list = option_list, usage = "This Script is a test for arguments!"))
   
   print(opt)
   ```

   注意，这个模块不用加上`-h`的flag，不然会报错：

   

   ```jsx
   Error in parse_args(OptionParser(option_list = option_list)) :
     Error in getopt(spec = spec, opt = args) :
     redundant long names for flags (column 1 of spec matrix).
   ```

   其实从这个报错信息可以看出来这个模块其实还是调用了`getopt`模块

   直接运行

   ```css
   Rscript test.R -f 10 -s Hello -t 1.1
   ```

   结果：

   ```php
   $first      
   [1] 10      
               
   $second     
   [1] "Hello" 
               
   $third      
   [1] 1.1     
               
   $help       
   [1] FALSE
   ```

   如果加上`-h`

   ```kotlin
   Usage: This Script is a test for arguments!     
                                                   
                                                   
   Options:                                        
           -f FIRST, --first=FIRST                 
                   This is first!                  
                                                   
           -s SECOND, --second=SECOND              
                   This is second!                 
                                                   
           -t THIRD, --third=THIRD                 
                   This is third!                  
                                                   
           -h, --help                              
                   Show this help message and exit 
   ```

   这里`Usage`后面跟的信息时候在OptionParser中`usage`方法指定。

   

   试了试这三个方式，第一个是最简洁的，如果你的脚本输出入参数很少（只有一两个的话），那就采用`commandArgs()`方法吧，使用轻便；其他两个模块干的活很相似，但是我感觉第三个`optparse`在接受参数的描述上与python的`optparse`模块的风格更加像一些，好像模块名也是一样的，里面的参数都很像，所以用过python的会对这个模块很熟悉，另外就是自动生成的帮助文档相对于`getopt`的来说更加友好一些，生成的解析命令行的描述不容易出错。但是就风格上，`getopt`更加轻便，解析命令行的参数的描述就是一个矩阵，看起来更加清晰一些。

   

   #### 数据保存

   保存数据有3中方式

   - cat函数

     cat(...,file="",sep="",fill = FALSE ,labels=NULL,append=FALSE) #file表示要输出的文件名，当参数append=TRUE时，在指定文件的末尾添加内容。sep：表示以空格作为分隔符

   - 保存文本文件

     write仅可以写出一个矩阵或向量的特定列，和对一个矩阵进行转置

     write.table() 可以把一个数据框或列表等对象以包含行列标签的方式写出。

     write.csv(): 将数据框保存为逗号分隔文件，但是不包含列名

   - 保存R格式文件

     save(data,file="test.Rdata")（保存的是整个环境里面的变量，调用的时候会将这个里面的变量重新载入到现有环境）

   - saveRDS(推荐)

   #### 图片保存

   保存图片有很多方法，我最常使用的是png(),pdf()函数和ggplot的ggsave函数

   - pdf函数

     在运行画图命令前，运行pdf函数，打开画布；画图完成后使用 dev.off()关闭画布

   - png函数

     在运行画图命令前，运行png函数，打开画布；画图完成后使用 dev.off()关闭画布

   - ggsave函数

     在使用ggplot2函数作图时候，将所有的画图命令存成一个变量，如p；再使用ggasve("file.png",p)进行画图；

     也可以使用下面的方法保存:

     ```{r}
     ggplot(mtcars, aes(wt, mpg)) + geom_point()
     ggsave("myplot.pdf")
     ggsave("myplot.png")
     ```

   #### 一页多图

   - 可以使用eaGgplot2包实现，例子：

   ```{r}
   library(easyGgplot2)
   # data.frame
   df <- ToothGrowth
   # 自定义框图与中心点图
   plot1<-ggplot2.boxplot(data=df, xName='dose',yName='len', groupName='dose', addDot=TRUE, dotSize=1, showLegend=FALSE)
   # 带中心点图的自定义点图
   plot2<-ggplot2.dotplot(data=df, xName='dose',yName='len', groupName='dose',showLegend=FALSE)
   # 带有中心点图的自定义带状图
   plot3<-ggplot2.stripchart(data=df, xName='dose',yName='len', groupName='dose', showLegend=FALSE)
   # Notched box plot
   plot4<-ggplot2.boxplot(data=df, xName='dose',yName='len', notch=TRUE)
   #在同一页上的多个图表,每一行展示两张图
   ggplot2.multiplot(plot1,plot2,plot3,plot4, cols=2)
   ```

   - 可以使用ggarrange函数实现，例子

     ```R
     library(ggpubr)
     #两行展示图形，并且给每个图添加了一个标签
     ggarrange(p1,p2,p3,ncol=2,nrow=2,labels=c("A","B","C"))
     ```

   - patchwork(超强推荐)

     **[有兴趣的可以查看这个链接，上面有详细的中文说明，很强大的功能](https://www.jianshu.com/p/622fff119eb4)**

     仅仅靠'+'符号便可以完成子图组合

     可以通过添加plot_layout（）调用来指定画布的布局。 可以定义网格的尺寸以及分配给不同行和列的空间大小

     可以通过将部分子图包装在括号中来制作嵌套布局。在这种情况下，画布布局的范围为不同的嵌套级别。

     ```R
     library(ggplot2)
     library(patchwork)
     
     p1 <- ggplot(mtcars) + geom_point(aes(mpg, disp))
     p2 <- ggplot(mtcars) + geom_boxplot(aes(gear, disp, group = gear))
     
     p1 + p2 + plot_layout(ncol = 1, heights = c(3, 1))
     ```

#### 命令行运行的优势

- 结合其他编程语言（python或者perl），快速批量的生成大量图片或者完成计算
- 方便脚本自动化




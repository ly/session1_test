## R包安装和管理

```{r}
#最常规安装
install.packages("doSNOW")

#从archive中安装.
install.packages("https://cran.r-project.org/src/contrib/doSNOW_1.0.18.tar.gz", repos=NULL, type="source")


#从文件夹安装
devtools::install_local("本地package路径")
devtools::install_local("/home/liangyong/session1/doSNOW_1.0.18.tar.gz")

#从gitlab上安装.
devtools::install_git("git@gitlab.com:xbiome/system-development/xviz.git", 
                      branch = "master", 
                      credentials = creds)

#从github上安装.
pacman::p_install_gh()
devtools::install_github()

#pacman 载入包,  如果这个包不存在，则自动install.packages()
pacman::p_load()    

#删除R包
remove.packages("doSNOW")
```

R包安装和管理有以下几种常用的方法：

+ install.packages()
+ BiocManager
+ pacman
+ devtools

下面是这些方法的详细介绍。

#### install.packages()

可以下载CRAN镜像上的R包



#### BiocManger

Bioconductor是基因组数据分析相关的软件包仓库，需要用专门的命令进行安装。

BiocManager::install()是最新版本的R和 Bioconductor安装Bioconductor 软件包的命令。



#### pacman

**pacman**包是一个整合了基础包`library`相关函数的包，用于更方便地对R包进行管理。该包可以添加到**`.Rprofile`**以便于显式地指定该包的函数，增加工作效率。其中关键函数`p_load`可以很好地提升论坛提问与博文，它会自动加载包，如果没有找到，会自动安装缺失包。

pacman包中的函数都以`p_xxx`的格式存在，其中`xxx`是函数执行的功能。比如`p_load`允许用户载入一个或多个包，用于替换`library`或`require`函数，如果包不在本地存在，它会自动为你安装。对于大多数的pacman函数，参数不需要指定为字符串。

| acman函数           | Base等价                              | 描述               |
| ------------------- | ------------------------------------- | ------------------ |
| `p_load`            | `install.packages` + `library`        | 载入与安装包       |
| `p_install`         | `install.packages`                    | 从CRAN安装         |
| `p_load_gh`         | NONE                                  | 载入和安装Github包 |
| `p_install_gh`      | NONE                                  | 从Github安装包     |
| `p_install_version` | `install.packages` & `packageVersion` | 安装包的最小版本   |
| `p_temp`            | NONE                                  | 暂时安装包         |
| `p_unload`          | `detach`                              | 从搜索路径卸下包   |
| `p_update`          | `update.packages`                     | 更新过期包         |
| p_loaded            | .packages` & `sessionInfo             | 列出已加载包       |



#### devtools

devtools::install_github()






## R多线程

什么时候需要用到并行计算？

在有时候的并行很简单，就是同一套操作流程，只是输入的文件或者数据不同。

例子A：

将一批货物从a地运往b地，如果只用一辆车，很费时需要拉10趟；如果10辆车同时运输，则只需要拉一趟？

例子B：

10个样品的reads，都需要使用fastp进行过滤，如果只用一个线程，需要10分钟；如果使用多个线程同时运行，则运行时间就小于10分钟。



```ruby
文件1    文件2    文件3
或       或       或
数据1    数据2    数据3
 |        |        |
 +----+   |   +----+
      |   |   |
      v   v   v
    +------------+
    |    一套     |
    |    处理     |  function
    |    方式     |
    +------------+
      |   |   |
  +---+   |   +----+
  |       |        |
  v       v        v
结果1    结果2    结果3
```

```{R}
fun <- function(x){
    return (x+1);
}
```

R语言常用实现并行计算的包有multicore、snow、snowfall、doParallel、doSNOW、parallel等等，另 外R语言也提供了与MongoDB、Hive、HBase、Hadoop、Redis等操作相关的包。有兴趣的可以查看  [R并行计算](https://dd2.douding.cn/getpath/down?key=%2Bmwhc3JMn7SmMSmkdTGgZUk7pOi9LtM0chDsSRfMjVY%3DH_TTHd%2Frr624%2FGPAlQVlHDtnVvamGZgnfj7SfarF68%2BtEDmPBEsPVM6X05iFZd4Iqwp&extname=pdf&filename=%25E3%2580%259028%25E3%2580%2591R%25E8%25AF%25AD%25E8%25A8%2580%25E5%25B9%25B6%25E8%25A1%258C%25E8%25AE%25A1%25E7%25AE%2597%25E5%25AE%259E%25E6%2588%2598.pdf)

![](https://i.loli.net/2020/09/14/4ov8ERWJ3OFtqu5.png)



![](https://i.loli.net/2020/09/14/TyaEpcAnQlL9qGv.png)



但今天，我们主要讲lapply函数、parallel包、foreach+doSNOW 三者的区别。

#### 使用lapply()

lapply()和parLapply()一样，输入都是一个列表，结果会返回一个新的列表（因为列表能包纳各种数据结构）

```{}
system.time({
  res <- lapply(1:1000000,fun);
})
```


#### 使用 `parallel`包的parLapply函数

```{R}
library(parallel)
c1 <- makeCluster(5)
system.time({
  parLapply(c1,1:1000000,fun);
})
stopCluster(c1)
```

| 函数            | 作用                   |
| --------------- | ---------------------- |
| detectCores()   | 检查当前的可用核数     |
| clusterExport() | 配置当前环境           |
| makeCluster()   | 分配核数               |
| stopCluster()   | 关闭集群               |
| parLapply()     | lapply()函数的并行版本 |

使用步骤：

```R
test_funciton <- function(file){
    # 导入某某包
    library(packages)
    # 读取
    raw.data <- read.table(file)
    data <- somefunction(raw.data)
    # 存为文件
    write.csv(data, "out.csv")
}

#首先可以查看一下机器的核数
detectCores()
# 这里初始化8个核
cl <- makeCluster(8)
#除了上述在函数体中加载包之外，也可以在事先用parallel的专门的函数进行加载
clusterExport(cl, library(packages))
#开始多线程
parLapply(cl, c(file1, file2, file3), test_funciton)
#运行完毕之后，需要释放，不然会一直占据资源
stopCluster(cl)
```



#### `doSNOW`包 + `foreach`包

其优势为：能转换输出结果的格式。

参数解读：

      （1）%do%严格按照顺序执行任务（所以，也就非并行计算），%dopar%并行执行任务，%do%时候就像sapply或lapply，%dopar%就是并行启动器
    
      （2）.combine：运算之后结果的显示方式，default是list，“c”返回vector， cbind和rbind返回矩阵，"+"和"*"可以返回rbind之后的“+”或者“*”，帮你把数据整合起来，太良心了！！
      （3）.init：.combine函数的第一个变量
      （4）.final：返回最后结果
      （5）.inorder：TRUE则返回和原始输入相同顺序的结果（对结果的顺序要求严格的时候），FALSE返回没有顺序的结果（可以提高运算效率）。这个参数适合于设定对结果顺序没有需求的情况。
      （6）.muticombine：设定.combine函数的传递参数，default是FALSE表示其参数是2，TRUE可以设定多个参数
      （7）.maxcombine：设定.combine的最大参数
      （8）.errorhandling：如果循环中出现错误，对错误的处理方法
      （9）.packages：指定在%dopar%运算过程中依赖的package（%do%会忽略这个选项），用于并行一些机器学习算法。
      （10）.export:在编译函数的时候需要预先加载一些内容进去，类似parallel的clusterExport

怎么调用：
```{r}
foreach (i=vector, .combine=’fun’) %dopar% {
code
return(object)
}
```
fun 可以等于 c,cbind,rbind,+,* 。 它们分别代表如下：

| fun值 | 解释                               |
| ----- | ---------------------------------- |
| c     | 返回一个list                       |
| cbind | 将这个list横向合并，返回一个数据框 |
| rbind | 将这个list纵向合并，返回一个数据框 |
| +     | 将这个返回的list相加，并返回该值   |
| *     | 将这个返回的list相乘，并返回该值   |

例子：
```{r}
library(doSNOW)
NumberOfCluster <- 4
cl <- makeCluster(NumberOfCluster)
registerDoSNOW(cl)
```

默认值（c）:
```{r}
x <- foreach(i = 1:5, .combine = "c") %dopar% {
y <- (1:5)^i
return(y)
}
x
```
cbind：

```{r}
x <- foreach(i = 1:5, .combine = "cbind") %dopar% {
y <- (1:5)^i
return(y)
}
x
```

例子如下（rbind）：

```{r}
x <- foreach(i = 1:5, .combine = "rbind") %dopar% {
y <- (1:5)^i
return(y)
}
x
```


```{R}

#使用 %dopar%实现并行计算
#使用 %do% 实现的单线程计算

#例
system.time(foreach(i=1:4) %do% Sys.sleep(3))
system.time(foreach(i=1:4) %dopar% Sys.sleep(3))
```

```{r}
#结束
stopCluster(cl)
```



影响多线程的时间有很多原因：

- **集群有其他任务** 如果10个cpu并行，其中一个cpu有其他任务在运行，就会影响这个CPU的时间，进而影响整个步骤的运行时间
- 系统调用函数的时间
- **CPU间的通讯时间**，不是CPU设置得越多越好，设置的越多，通讯时间就越多。（如：10个cpu并行计算，单个cpu用时10ms，但每个cpu的通讯时间1ms，则整个时间为20ms）。**CPU的设置需要权衡当前集群的CPU状态和通讯时间**。 

